var express = require('express');
var router = express.Router();
var instrumentsRouter = require('./instruments');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/instruments', instrumentsRouter);

module.exports = router;
