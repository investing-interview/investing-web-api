var express = require('express');
var router = express.Router();

const {InstrumentsStorage} = require("../models");

/**
 * Get all the different instruments
 */
router.get('/', (req, res, next) => {
    InstrumentsStorage.findAll()
        .then(data => {
            res.json({success: true, data})
        }).catch(err => next(err));
});


/**
 * Create new instrument
 * @body id
 * @body type
 * @body name
 * @body symbol
 */
router.post('/', (req, res, next) => {
    const {id, type, name, symbol} = req.body;

    InstrumentsStorage.findByPk(id)
        .then(entry => {
            if (entry) {
                return res.status(400).json({success: false, msg: `instrument with id:${id} already exists`})
            }
            return InstrumentsStorage.create({instrumentType: type, instrumentId: id, name, symbol})
        })
        .then(resp => {
            if (resp) {
                res.json({success: true, msg: `added new instrument id:${id} successfully`, data: resp})
            }
        }).catch(err => next(err));
});


/**
 * Delete a instrument by given id
 * @param id - the instrument to remove
 */
router.delete('/:id', (req, res, next) => {
    const {id} = req.params;
    InstrumentsStorage.destroy({where: {instrumentId: id}})
        .then(resp => {
            if (resp) {
                return res.json({success: true, msg: `the instrument id:${id} has been deleted`})
            } else {
                return res.status(400).json({success: false, msg: `no instrument with id:${id} exists`});
            }
        })
        .catch(err => next(err));
});


module.exports = router;
