const { Sequelize } = require('sequelize');
const config = require('../config/dbKeys');

const db = new Sequelize(`postgres://${config.username}:${config.password}@${config.domain}:5432/${config.tableName}`);

db.authenticate()
    .then(() => console.log('Connection has been established successfully.')
    )
    .catch((error) => {
        console.error('Unable to connect to the database:', error);
    });

module.exports = db;