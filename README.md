# `investing-web-api`
An HTTP server handle the access to DB, and resolve actions from the different components.
Build according to REST API principles.

### How to run - instructions:
1. clone the repo to your working space
2. `cd invensting-web-api`
3. run on the terminal `npm install package.json`
4. start the application by `npm start`

## Endpoints:
For further documentation and examples - see the `/routes` folder.

### Intruments
* Request initialise `api/instruments/`
* Create new instrument entry
* Delete existing instrument entry
* Get list of all existing instrument entries    

## Stack:
* Node.js\Express
* Libs: [sequelize](https://sequelize.org/)
* DB: postgreSQL on [ElephantSQL](https://elephantsql.com)