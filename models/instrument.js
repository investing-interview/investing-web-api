const {Model, DataTypes} = require("sequelize");

const db = require('../services/postgreDBService');

class Instrument extends Model {}

Instrument.init({
    // Model attributes are defined here
    instrumentId: {
      type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        field: 'instrumentid'
    },
    name: {
        type: DataTypes.CHAR(41),
        allowNull: false,
    },
    symbol: {
        type: DataTypes.CHAR(7),
        allowNull: false
    },
    instrumentType: {
        type: DataTypes.CHAR(9),
        allowNull: false,
        field: "instrumenttype"
    }
}, {
    // Other model options go here
    sequelize: db, // the connection instance
    modelName: 'instrument', // We need to choose the model name
    tableName: 'instrument',
    timestamps: false
});

module.exports = Instrument;